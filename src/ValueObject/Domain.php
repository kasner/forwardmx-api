<?php declare(strict_types=1);

namespace ForwardMX\ValueObject;

class Domain
{
    /** @var string */
    private $domain;

    /** @var string */
    private $status;

    public function __construct(string $domain, string $status)
    {
        $this->domain = $domain;
        $this->status = $status;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
