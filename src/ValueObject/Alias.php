<?php declare(strict_types=1);

namespace ForwardMX\ValueObject;

class Alias
{
    /** @var string */
    private $alias;

    /** @var string */
    private $source;

    /** @var string */
    private $destination;

    /** @var int */
    private $count;

    public function __construct(string $source, string $destination, int $count)
    {
        $this->alias = substr($source, 0, -1);
        $this->source = $source;
        $this->destination = $destination;
        $this->count = $count;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
